package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {

  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)

    val exampleString = "aaaaaaaabbbcdefgh"
    val toDecodeExample = List(1, 0, 0, 0, 1, 0, 1, 0)
    val decodedExample = List('b', 'a', 'c')

    val leafA = Leaf('a', 8)
    val leafB = Leaf('b', 3)
    val treeCD = Fork(Leaf('c', 1), Leaf('d', 1), List('c', 'd'), 2)
    val treeEF = Fork(Leaf('e', 1), Leaf('f', 1), List('e', 'f'), 2)
    val treeGH = Fork(Leaf('g', 1), Leaf('h', 1), List('g', 'h'), 2)
    val treeBCD = Fork(leafB, treeCD, List('b', 'c', 'd'), 5)
    val treeEFGH = Fork(treeEF, treeGH, List('e', 'f', 'g', 'h'), 4)
    val treeBCDEFGH = Fork(treeBCD, treeEFGH, List('b', 'c', 'd', 'e', 'f', 'g', 'h'), 9)
    val exampleTree = Fork(leafA, treeBCDEFGH, List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'), 17)

    val exampleCodeTable: CodeTable = List(
      ('a', List(0)), ('b', List(1, 0, 0)), ('c', List(1, 0, 1, 0)), ('d', List(1, 0, 1, 1)), ('e', List(1, 1, 0, 0)),
      ('f', List(1, 1, 0, 1)), ('g', List(1, 1, 1, 0)), ('h', List(1, 1, 1, 1))
    )
  }


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("weight of an example tree") {
    new TestTrees {
      assert(weight(exampleTree) === 17)
    }
  }

  test("weight of a leafA") {
    new TestTrees {
      assert(weight(leafA) === 8)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("chars of an example tree") {
    new TestTrees {
      assert(chars(exampleTree) === List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'))
    }
  }

  test("chars of a leafB") {
    new TestTrees {
      assert(chars(leafB) === List('b'))
    }
  }

  test("makeCodeTree of treeEF and treeGH") {
    new TestTrees {
      assert(makeCodeTree(treeEF, treeGH) === treeEFGH)
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times of 'aba'") {
    new TestTrees {
      assert(times(List('a', 'b', 'a')) === List(('a', 2), ('b', 1)))
    }
  }

  test("times of 'ccccdccdccdccdeefff'") {
    new TestTrees {
      assert(times(string2Chars("ccccdccdccdccdeefff")) === List(('c', 10), ('d', 4), ('e', 2), ('f', 3)))
    }
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("singleton of list of two trees") {
    new TestTrees {
      assert(singleton(List(treeEF, treeGH)) === false)
    }
  }

  test("singleton of list with single tree") {
    new TestTrees {
      assert(singleton(List(treeEF)) === true)
    }
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of list with leafB, treeCD, treeEF, treeGH") {
    new TestTrees {
      val leafList = List(leafB, treeCD, treeEF, treeGH)
      assert(combine(leafList) === List(treeBCD, treeEF, treeGH))
    }
  }

  test("until of list with leafB, treeCD, treeEF, treeGH") {
    new TestTrees {
      val leafList = List(leafB, treeCD, treeEFGH)
      assert(until(singleton, combine)(leafList) === List(treeBCDEFGH))
    }
  }

  //  test("createCodeTree for exampleString") {
  //    new TestTrees {
  //      assert(createCodeTree(string2Chars(exampleString)) === exampleTree)
  //    }
  //  }

  test("decode example with example tree") {
    new TestTrees {
      assert(decode(exampleTree, toDecodeExample) === decodedExample)
    }
  }

  test("decode secret") {
    assert(decodedSecret.mkString("") === "huffmanestcool")
  }

  test("encode example with example tree") {
    new TestTrees {
      assert(encode(exampleTree)(decodedExample) === toDecodeExample)
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("decode and encode secret using frenchCode should be identity") {
    new TestTrees {
      assert(decode(frenchCode, encode(frenchCode)("huffmanestcool".toList)) === "huffmanestcool".toList)
    }
  }

  test("convert example tree into example CodeTable") {
    new TestTrees {
      assert(convert(exampleTree) === exampleCodeTable)
    }
  }

  test("codeBits for 'd' using example CodeTable") {
    new TestTrees {
      assert(codeBits(exampleCodeTable)('d') === List(1, 0, 1, 1))
    }
  }

  test("codeBits for 'x' using example CodeTable") {
    new TestTrees {
      assert(codeBits(exampleCodeTable)('x') === List())
    }
  }

  test("quickEncode example with example tree") {
    new TestTrees {
      assert(quickEncode(exampleTree)(decodedExample) === toDecodeExample)
    }
  }

  test("quickEncode secret using frenchCode") {
    new TestTrees {
      assert(quickEncode(frenchCode)("huffmanestcool".toList) === secret)
    }
  }
}
