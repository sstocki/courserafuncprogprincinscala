package silverstar
package coursera.functional.programming
package week3

/**
  * Set of integers.
  */
abstract class IntSet1 {

  def includes(x: Int): IntSet1

  def contains(x: Int): Boolean

  def union(other: IntSet1): IntSet1
}

class Empty1 extends IntSet1 {

  override def includes(x: Int): IntSet1 = new NonEmpty1(x, new Empty1, new Empty1)

  override def contains(x: Int): Boolean = false

  override def toString: String = "."

  override def union(other: IntSet1): IntSet1 = other
}

class NonEmpty1(elem: Int, left: IntSet1, right: IntSet1) extends IntSet1 {

  override def includes(x: Int): IntSet1 = {
    if (x < elem) new NonEmpty1(elem, left.includes(x), right)
    else if (x > elem) new NonEmpty1(elem, left, right.includes(x))
    else this
  }

  override def contains(x: Int): Boolean = {
    if (x < elem) left.contains(x)
    else if (x > elem) right.contains(x)
    else true
  }

  override def toString: String = s"{$left $elem $right}"

  override def union(other: IntSet1): IntSet1 = left.union(right).union(other).includes(elem)

}

object IntSet1Runner {

  def main(args: Array[String]) {

    val s1 = new NonEmpty1(3, new Empty1, new Empty1)
    val empty = new Empty1
    println(s"s1=$s1")
    println(s"s1.includes(4)=${s1.includes(4)}")
    println(s"empty.union(s1)=${empty.union(s1)}")
    println(s"s1.union(empty)=${s1.union(empty)}")
  }
}