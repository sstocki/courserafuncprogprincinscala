package recfun

import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {

    def iterate(col: Int, row: Int): Int = {
      if (col == 0 || col == row) 1
      else if (row == 1) 1
      else iterate(col - 1, row - 1) +  iterate(col, row - 1)
    }

    if(c >= 0 && r >= 0 && c <= r) iterate(c, r)
    else 0
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    @tailrec
    def iterate(charList: List[Char], balance: Int): Int = {
      charList match {
        case List() => balance
        case c :: tail => {
          val balancingFactor: Int = balance + {
            if (c == '(') 1
            else if (c == ')') -1
            else 0
          }
          if(balancingFactor < 0) balancingFactor
          else iterate(tail, balancingFactor)
        }
      }
    }

    iterate(chars, 0) == 0
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {

    def iterate(moneyLeft: Int, coinsToUse: List[Int], numberOfWays: Int): Int = {

      if(moneyLeft == 0) numberOfWays + 1
      else if(coinsToUse.isEmpty || moneyLeft < 0) numberOfWays
      else {
        iterate(moneyLeft - coinsToUse.head, coinsToUse, numberOfWays) + iterate(moneyLeft, coinsToUse.tail, numberOfWays)
      }
    }

    if(money == 0 || coins.isEmpty) 0
    else iterate(money, coins, 0)
  }
}
