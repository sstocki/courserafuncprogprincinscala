package funsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  // test("string take") {
  //   val message = "hello, world"
  //   assert(message.take(5) == "hello")
  // }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  // test("adding ints") {
  //   assert(1 + 2 === 3)
  // }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
    val sss = (x: Int) => x > 900
    val ttt = (x: Int) => x >= 3 && x <= 8
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("singleton(2) does not contain 1") {
    new TestSets {
      assert(!contains(s2,1), "Singleton(2)")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersection contains common elements of two set") {
    new TestSets {
      val s = union(s1, s2)
      val t = intersect(s, s1)
      assert(contains(t, 1), "Intersection 1")
      assert(!contains(t, 2), "Intersection 2")
    }
  }

  test("intersection of separable sets return empty set") {
    new TestSets {
      val t = intersect(s1, s2)
      assert(!contains(t, 1), "Intersection 1")
      assert(!contains(t, 2), "Intersection 2")
    }
  }

  test("difference contains all elements from one set that are not present in other set") {
    new TestSets {
      val s: Set = union(s1, s2)
      val t: Set = diff(s, s1)
      assert(!contains(t, 1), "Intersection 1")
      assert(contains(t, 2), "Intersection 2")
    }
  }

  test("difference of separable sets returns first set") {
    new TestSets {
      val t: Set = diff(s1, s2)
      assert(contains(t, 1), "Intersection 1")
      assert(!contains(t, 2), "Intersection 2")
    }
  }

  test("filter contains all elements from one set that are accepted by a given predicate") {
    new TestSets {
      val s: Set = union(union(s1, s2), s3)
      val t: Set = filter(s, (x: Int) => x > 2)
      assert(!contains(t, 1), "Intersection 1")
      assert(!contains(t, 2), "Intersection 2")
      assert(contains(t, 3), "Intersection 3")
    }
  }

  test("filter of separable set and predicate returns empty set") {
    new TestSets {
      val s: Set = union(union(s1, s2), s3)
      val t: Set = filter(s, _ < 0)
      assert(!contains(t, 1), "Intersection 1")
      assert(!contains(t, 2), "Intersection 2")
      assert(!contains(t, 3), "Intersection 3")
    }
  }

  test("forall for numbers greater than 100 with positive numbers predicate") {
    new TestSets {
      val result: Boolean = forall(sss, _ > 0)
      assert(result, "Forall")
    }
  }

  test("forall for numbers greater than 100 with even numbers predicate") {
    new TestSets {
      val result: Boolean = forall(sss, _ % 2 == 0)
      assert(!result, "Forall")
    }
  }

  test("forall for empty set with even numbers predicate") {
    val result: Boolean = forall(_ => false, _ % 2 == 0)
    assert(!result, "Forall")
  }

  test("exists for singleton(2) with even numbers predicate") {
    new TestSets {
      val result: Boolean = exists(s2, _ % 2 == 0)
      assert(result, "Exists")
    }
  }

  test("exists for numbers greater than 900 with negative numbers predicate") {
    new TestSets {
      val result: Boolean = exists(sss, _ < 0)
      assert(!result, "Exists")
    }
  }

  test("exists for evenAnd3 numbers with odd numbers predicate") {
    new TestSets {
      val evenAnd3 = (x: Int) => x % 2 == 0 || x == 3
      val result: Boolean = exists(evenAnd3, _ % 2 == 1)
      assert(result, "Exists")
    }
  }

  test("exists for empty set with odd numbers predicate") {
    val result: Boolean = exists(_ => false, _ % 2 == 1)
    assert(!result, "Forall")
  }

  test("map for numbers <3, 8> with + 100 mapping function") {
    new TestSets {
      val t: Set = map(ttt, (x: Int) => x + 100)
      assert(contains(t, 103), "Map 12")
      assert(!contains(t, 5), "Map 10")
    }
  }
}
