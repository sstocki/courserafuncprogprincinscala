package silverstar
package coursera.functional.programming
package week3

import java.util.NoSuchElementException

/**
  * My list.
  */
trait MyList[T] {

  def isEmpty: Boolean
  def head: T
  def tail: MyList[T]

  def singleton(elem: T): MyList[T] = new MyCons[T](elem, new Nil[T])
}

class MyCons[T](val head: T, val tail: MyList[T]) extends MyList[T] {

  override def isEmpty: Boolean = false
}

class Nil[T] extends MyList[T] {

  override def isEmpty: Boolean = true

  override def tail: Nothing = throw new NoSuchElementException("Nil.tail")

  override def head: Nothing = throw new NoSuchElementException("Nil.head")
}

object MyListRunner {

  def nth[T](n: Int, list: MyList[T]): T = {
    if(list.isEmpty) throw new IndexOutOfBoundsException
    if(n == 0) list.head
    else nth(n -1, list.tail)
  }

  def main(args: Array[String]) {

    val list: MyList[Int] = new MyCons[Int](23, new MyCons[Int](41, new MyCons[Int](7, new MyCons[Int](9, new Nil[Int]))))

    val listIndex3 = nth(3, list)
    println(s"listIndex3=$listIndex3")
    println(s"listIndex4=${nth(4, list)}")
    println(s"listIndex-1=${nth(-1, list)}")
  }
}