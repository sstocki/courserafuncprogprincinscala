package silverstar
package coursera.functional.programming
package week3

/**
  * Set of integers.
  */
abstract class IntSet2 {

  def includes(x: Int): IntSet2

  def contains(x: Int): Boolean
}

object Empty2 extends IntSet2 {

  override def includes(x: Int): IntSet2 = new NonEmpty2(x, Empty2, Empty2)

  override def contains(x: Int): Boolean = false

  override def toString: String = "."
}

class NonEmpty2(elem: Int, left: IntSet2, right: IntSet2) extends IntSet2 {

  override def includes(x: Int): IntSet2 = {
    if (x < elem) new NonEmpty2(elem, left.includes(x), right)
    else if (x > elem) new NonEmpty2(elem, left, right.includes(x))
    else this
  }

  override def contains(x: Int): Boolean = {
    if (x < elem) left.contains(x)
    else if (x > elem) right.contains(x)
    else true
  }

  override def toString: String = s"{$left $elem $right}"
}

object IntSet2Runner {

  def main(args: Array[String]) {

    val s1 = new NonEmpty2(3, Empty2, Empty2)
    println(s"s1=$s1")
    println(s"s1.includes(4)=${s1.includes(4)}")
  }
}